package com.aysimaerdem.sgtsozluk

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class CategoryActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category)


        val siberGuvenlik = findViewById<Button>(R.id.siberguvenlik)
        siberGuvenlik.setOnClickListener {
            val intent = Intent(this,ListActivity::class.java)
            intent.putExtra("jsonName", "siber_guvenlik_sozluk.json")
            startActivity(intent)
        }

        val yazilimSozluk = findViewById<Button>(R.id.yazilim)
        yazilimSozluk.setOnClickListener {
            val intent2 = Intent(this,ListActivity::class.java)
            intent2.putExtra("jsonName", "yazilim_sozluk.json")
            startActivity(intent2)
        }

        val webSozluk = findViewById<Button>(R.id.web)
        webSozluk.setOnClickListener {
            val intent3 = Intent(this,ListActivity::class.java)
            intent3.putExtra("jsonName", "web_sozluk.json")
            startActivity(intent3)
        }
    }
}