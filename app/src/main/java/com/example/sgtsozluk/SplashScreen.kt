package com.aysimaerdem.sgtsozluk

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.*

class SplashScreen : AppCompatActivity() {
    val activityScope = CoroutineScope(Dispatchers.Main)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)

        activityScope.launch {
            delay(3000)

            val intent = Intent(this@SplashScreen, CategoryActivity::class.java)
            startActivity(intent)
            finish()
        }

    }

}

