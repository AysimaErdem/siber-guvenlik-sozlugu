package com.aysimaerdem.sgtsozluk

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers


class MainActivity : AppCompatActivity() {

    val activityScope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.splash_screen)

        activityScope.launch{
            delay(3000)

            val intent = Intent(this@MainActivity, CategoryActivity::class.java)
            startActivity(intent)
            finish()
        }

    }

}