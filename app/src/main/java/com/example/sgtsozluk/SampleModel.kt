package com.aysimaerdem.sgtsozluk

data class SampleModel
    (
    val id : Int,
    val name : String,
    val description: String
    )
