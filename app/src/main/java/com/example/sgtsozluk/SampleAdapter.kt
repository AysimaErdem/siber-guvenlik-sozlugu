package com.aysimaerdem.sgtsozluk

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog

class SampleAdapter(val sozlukList: List<SampleModel>, context: Context) :
    RecyclerView.Adapter<SampleAdapter.ViewHolder>() {

    private val context = context

    // Holds the views for adding it to image and text
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val baslikTextView: TextView = itemView.findViewById(R.id.baslik)
        val cardContainer: CardView = itemView.findViewById(R.id.cardContainer)
        // val aciklamaTextView: TextView = itemView.findViewById(R.id.aciklama)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cardview, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return sozlukList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.baslikTextView.text = sozlukList[position].name
        //holder.aciklamaTextView.text = sozlukList[position].description
        holder.itemView.setOnClickListener {
            prepareBottomDialog(sozlukList[position])
        }

        holder.cardContainer.setOnClickListener {
            prepareBottomDialog(sozlukList[position])
        }


    }

    fun prepareBottomDialog(clickItem: SampleModel) {
        val sheetDialog = BottomSheetDialog(context, R.style.BottomSheetDialog)
        sheetDialog.setContentView(R.layout.bottom_sheet_dialog)
        sheetDialog.setCancelable(true)

        val kelimeTextView: TextView? = sheetDialog.findViewById(R.id.kelime)
        val aciklamaDetayTextView: TextView? = sheetDialog.findViewById(R.id.aciklamadetay)

        kelimeTextView?.text = clickItem.name
        aciklamaDetayTextView?.text = clickItem.description

        sheetDialog.show()

    }
}
