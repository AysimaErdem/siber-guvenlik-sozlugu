package com.aysimaerdem.sgtsozluk

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.SearchView.OnQueryTextListener
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import androidx.cardview.widget.CardView

class ListActivity : AppCompatActivity() {
    lateinit var sozlukList: List<SampleModel>
    private lateinit var recyclerView: RecyclerView
    private lateinit var searchView: SearchView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sozluk)

        recyclerView = findViewById(R.id.recyclerview)
        searchView = findViewById(R.id.searchView)

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)



        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(searchText: String?): Boolean {
                println("aranan==> $searchText")
                sozlukList
                return true
            }


        })

        var intentData = intent.getStringExtra("jsonName")
        //"siber_guvenlik_sozluk.json"
        sozlukList = getList(intentData ?: "")

        bindAdapter()
    }




    fun getList(fileName: String): List<SampleModel>{
        val jsonFileString: String =Utils.getJsonFromAssets(
            applicationContext, fileName
        )
        Log.i("data", jsonFileString)

        val gson = Gson()
        val listUserType: Type = object : TypeToken<List<SampleModel?>?>() {}.type

        val list = gson.fromJson<List<SampleModel>>(jsonFileString, listUserType)
        val sortedList = (list as MutableList).sortedBy { it.name }

        return sortedList
    }

    fun bindAdapter(){
        val list = arrayListOf<String>("asdasd","asdasd","asds","23e23e","234234","234234234")
        val adapter = SampleAdapter(sozlukList, this)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = adapter

    }

    lateinit var cardviewdetails: CardView


}







